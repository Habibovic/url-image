package com.example.korisn.predavanje6imageurl;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {
    ImageView image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        image = (ImageView) findViewById(R.id.showImage);
        ContentDownloader contentDownloader = new ContentDownloader();
        //Contentn download on start
        try {
            String result = contentDownloader.execute("http://tanjir.ba").get();
            Log.i("Info", result);
        } catch (InterruptedException e) {
            Log.i("Info", "Cant download");
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    //Download image and show - with picasso
    public void downloadImagePicasso(View view) {
        Picasso.with(this)
                .load("https://crackberry.com/sites/crackberry.com/files/styles/large/public/topic_images/2013/ANDROID.png")
                .placeholder(R.drawable.images)
                .error(R.drawable.download)
                .into(image);
    }

    //Download image  show - with AsyncTask
    public void downloadImage(View view) {

        ImageDownloader task = new ImageDownloader();
        Bitmap myImage = null;

        try {
            myImage = task.execute("http://www.youngwebbuilder.com/wp-content/uploads/2013/05/android-lime-pie1-590x525.jpg").get();

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        if (myImage != null) {
            image.setImageBitmap(myImage);
        }
    }

    //Content Download with AsyncTask
    private class ContentDownloader extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = urlConnection.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder result = new StringBuilder();
                String line;
                while ((line = reader.readLine()) != null) {
                    result.append(line).append("\n");
                }

                return result.toString();


            } catch (IOException e) {
                e.printStackTrace();
                return "Bad request ";
            }
        }
    }

    private class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected Bitmap doInBackground(String... strings) {
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                InputStream in = connection.getInputStream();
                return BitmapFactory.decodeStream(in);

            } catch (IOException e) {
                e.printStackTrace();
            }


            return null;
        }
    }
}

